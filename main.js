import TreeNode from './classes/treeNode.js';
import HillClimbing from './classes/hillClimbing.js';

// Initial Col Of Each Row
const initialState = [
    0, 1, 2, 3, 4, 5, 6, 7
];

// Create Root Node
const rootNode = new TreeNode(null, initialState);

// Create Hill Climbing Search And Run
const hillClimbing = new HillClimbing(rootNode);
hillClimbing.findAnswer();
hillClimbing.printGoalState();
