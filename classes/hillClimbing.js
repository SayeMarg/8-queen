export default class HillClimbing {
    constructor (rootNode) {
        this.rootNode = rootNode;

        this.goalNode = null;
    }

    // To Find Nearest Answer
    findAnswer () {
        let currentNode = this.rootNode;
        currentNode.setFunctionality();

        while (true) {
            currentNode.setChildren();

            // Set Functionality Of Current Node Children
            for (let item of currentNode.child) {
                item.setFunctionality();
            }

            // Sort Children By Functionality
            currentNode.child.sort((first, second) => first.functionality - second.functionality);

            // Check That Current Node Is Best Of That Children
            if (currentNode.functionality < currentNode.child[0].functionality) {
                this.goalNode = currentNode;
                break;
            }

            // If Not -> Go To Next Best Child
            currentNode = currentNode.child[0];
        }
    }

    // Print Goal State
    printGoalState () {
        for (let row = 0; row < 8; row++) {
            console.log(`Row ${row + 1}: ${this.goalNode.state[row] + 1}`);
        }
    }
}
