export default class TreeNode {
    constructor (parentNode, state) {
        this.parentNode = parentNode;
        this.state = state;

        // List Of Child Nodes
        this.child = [];

        // Depth = Row
        this.depth = (this.parentNode) ? (this.parentNode.depth + 1) : 0;
    }

    // Set Value Of Functionality
    setFunctionality () {
        let functionality = 0;

        for (let i = 0; i < 8; i++) {
            for (let j = i; j < 8; j++) {
                if ((this.state[i] === this.state[j]) || (i - this.state[i] === j - this.state[j])) {
                    functionality++;
                }
            }
        }

        this.functionality = functionality;
    }

    // Copy Current State
    copyState () {
        return [...this.state];
    }

    // Set Children Of This Node
    setChildren () {
        let newState;

        for (let i = 1; i <= 7; i++) {
            newState = this.copyState();

            // Move Current State One Col
            newState[this.depth] = (newState[this.depth] + i) % 8;

            this.child.push(new TreeNode(this, newState));
        }
    }
}
